-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 05 jan. 2022 à 20:45
-- Version du serveur : 8.0.27
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `modules`
--

-- --------------------------------------------------------

--
-- Structure de la table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `historiques`
--

DROP TABLE IF EXISTS `historiques`;
CREATE TABLE IF NOT EXISTS `historiques` (
  `fk_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `fk_nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_date_debut` date NOT NULL,
  `valeur_mesuree_actuelle` double NOT NULL,
  `date_fin` date NOT NULL,
  `duree_fonctionnement` double NOT NULL,
  `donnee_envoyee` double NOT NULL,
  `etat_de_marche` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`fk_id`),
  KEY `historiques_fk_nom_foreign` (`fk_nom`),
  KEY `historiques_fk_date_debut_foreign` (`fk_date_debut`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `historiques`
--

INSERT INTO `historiques` (`fk_id`, `fk_nom`, `fk_date_debut`, `valeur_mesuree_actuelle`, `date_fin`, `duree_fonctionnement`, `donnee_envoyee`, `etat_de_marche`, `created_at`, `updated_at`) VALUES
(1, 'hp', '2021-12-29', 15, '2021-12-30', 30, 8, 1, '2021-12-30 06:23:24', '2021-12-30 06:23:24'),
(2, 'iphone', '2021-12-29', 19, '2021-12-30', 20, 3, 1, '2021-12-30 06:23:24', '2021-12-30 06:23:24'),
(3, 'itel', '2022-01-05', 204, '2022-01-26', 499, 205, 1, '2022-01-06 03:38:43', '2022-01-06 03:38:43'),
(4, 'itel', '2022-01-05', 367, '2022-02-01', 416, 463, 1, '2022-01-06 03:43:07', '2022-01-06 03:43:07'),
(5, 'kaaris', '2022-01-06', 296, '2022-01-28', 380, 240, 1, '2022-01-06 03:44:05', '2022-01-06 03:44:05'),
(6, 'blackberry', '2022-01-05', 161, '2022-01-19', 277, 147, 1, '2022-01-06 03:56:07', '2022-01-06 03:56:07');

-- --------------------------------------------------------

--
-- Structure de la table `inscriptions`
--

DROP TABLE IF EXISTS `inscriptions`;
CREATE TABLE IF NOT EXISTS `inscriptions` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `userlastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `userfirstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usermail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `userpass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `inscriptions_usermail_unique` (`usermail`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `inscriptions`
--

INSERT INTO `inscriptions` (`id`, `userlastname`, `userfirstname`, `usermail`, `userpass`, `created_at`, `updated_at`) VALUES
(1, 'DOVI', 'Sanvee', 'cedricdovi6@gmail.com', 'cedric', '2021-12-30 21:08:48', '2021-12-30 21:08:48'),
(2, 'DOVI', 'Jovani', 'jovanidovi1999@gmail.com', 'jovani', '2021-12-30 21:09:15', '2021-12-30 21:09:15'),
(3, 'ATTIVON', 'Sandro Koffi', 'sandrodovi1999@gmail.com', 'sandro', '2021-12-31 07:50:07', '2021-12-31 07:50:07');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2021_12_24_200557_create_modules_table', 1),
(6, '2021_12_27_204436_historiques', 1),
(7, '2021_12_28_174024_inscription', 1);

-- --------------------------------------------------------

--
-- Structure de la table `modules`
--

DROP TABLE IF EXISTS `modules`;
CREATE TABLE IF NOT EXISTS `modules` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_debut` date NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `modules`
--

INSERT INTO `modules` (`id`, `nom`, `date_debut`, `description`, `created_at`, `updated_at`) VALUES
(1, 'hp', '2021-12-29', 'pavillon', '2021-12-30 06:23:24', '2021-12-30 06:23:24'),
(2, 'iphone', '2021-12-29', 'Xr 64Gb', '2021-12-30 06:23:24', '2021-12-30 06:23:24'),
(42, 'itel', '2022-01-05', 'core5', '2022-01-06 03:43:07', '2022-01-06 03:43:07'),
(44, 'blackberry', '2022-01-05', 'version 5', '2022-01-06 03:56:07', '2022-01-06 03:56:07'),
(32, 'lg', '2022-01-05', 'led', '2022-01-06 03:14:29', '2022-01-06 03:14:29'),
(33, 'lg', '2022-01-05', 'led', '2022-01-06 03:15:18', '2022-01-06 03:15:18'),
(34, 'lg', '2022-01-06', 'led', '2022-01-06 03:15:54', '2022-01-06 03:15:54'),
(35, 'lg', '2022-01-06', 'led', '2022-01-06 03:16:48', '2022-01-06 03:16:48'),
(36, 'lg', '2022-01-06', 'led', '2022-01-06 03:23:45', '2022-01-06 03:23:45'),
(37, 'lg', '2022-01-05', 'led', '2022-01-06 03:28:14', '2022-01-06 03:28:14'),
(38, 'lg', '2022-01-05', 'led', '2022-01-06 03:28:42', '2022-01-06 03:28:42');

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
