<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\module;
use App\Models\historique;
class modules extends Controller
{
    //fonction store qui permet l'insertion e données dans les table modules et historiques 
    public function store(Request $request){
      
        // stockage dans une variable des fonction issue du formulaire
        $name = request('nom');
        $description = request('description');
        $date_debut = request('date_debut');
        //creation de differente variable et affectation de donné
        $etat =  1;
        $donne_envoyee = rand(1,500);
        $valeur_mesuree = rand(1,500);
        $duree_fonctionnement = rand(1,500);
        $date_fin = date('Y-m-d', strtotime( '+'.mt_Rand(0,30).' days'));
        //creation d'une nouvelle ligne de module et insertion de cette ligne
        $Modules = new module();
        $Modules->nom = $name;
        $Modules->description = $description;
        $Modules->date_debut = $date_debut;
        //creation d'une nouvelle ligne d'historique et insertion de cette ligne 
        $Historique = new historique();
        $Historique->fk_nom = $name;
        $Historique->valeur_mesuree_actuelle= $valeur_mesuree;
        $Historique->etat_de_marche = $etat;
        $Historique->fk_date_debut = $date_debut;
        $Historique->donnee_envoyee =  $donne_envoyee;
        $Historique->duree_fonctionnement = $duree_fonctionnement;
        $Historique->date_fin = $date_fin;
        //souvegarde des lignes respectifs dans leur table
        $Modules->save();
        $Historique->save();
        return back();
     }
   
}
