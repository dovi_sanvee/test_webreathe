<?php

session_start();
use App\Http\Controllers\modules;
use App\Http\Controllers\ModulesController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// retourne la view welcome
Route::get('/', function () {
    return view('welcome');
});
// routeur pour la liste des modules
Route::get('/listModules', 'App\Http\Controllers\ModulesController@listModules');
//routeur de la page modules
Route::get('/Modules', 'App\Http\Controllers\ModulesController@Modules');
//routeur d'ajout de modules 
Route::post('/Modules', 'App\Http\Controllers\ModulesController@addModules');
//routeur d'inscription  
Route::post('/signin', 'App\Http\Controllers\Account@accountsignin');
//routeur de login
Route::post('/login', 'App\Http\Controllers\Account@accountlogin');

// routeur  pour le  logout
Route::get('/logout', 'App\Http\Controllers\Account@logout');
// routeur d'ajout des modules et historiques 
Route::post('/Modules', [modules::class,'store']);










