<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" >
        <link rel="stylesheet"  type="text/css" href="../../css/app.css">
        <link  rel="stylesheet" href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" >
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
        <title>Acceuil</title>
       
    </head>
    </head>
    <body>

        <nav class="navbar navbar-dark bg-primary">
            <a class="navbar-brand" href="/">Log in</a>
        </nav>
         
        <div id="account">
 <!--Formulaire de login -->

<form class="account-login" method="post" action="/login">
@csrf
    <h2>Connexion</h2>
    <h3>Tu as déjà un compte ?</h3>

    <p>Adresse mail</p>
    <input type="text" name="usermail" placeholder="Adresse mail" />

    <p>Mot de passe</p>
    <input type="password" name="userpass" placeholder="Mot de passe" />

    <input type="submit" class="btn btn-primary"value="Connexion" />

</form>
 <!--Formulaire de signin -->

<form class="account-signin" id="formSaisi" method="post" action="/signin">
{{csrf_field()}}

    <h2>Inscription</h2>
    <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

    <p>Nom</p>
    <input    type="text" name="userlastname"  id="nom" placeholder="Nom"  />

    <p>Prénom</p>
    <input type="text" name="userfirstname"   id="prenom"  placeholder="Prénom" />

    <p>Adresse mail</p>
    <input type="text" name="usermail"   id="mail"  placeholder="Adresse mail" />

    <p>Mot de passe</p>
    <input type="password" name="userpass"  id="motdepasse" placeholder="Mot de passe" />

    <p>Répéter le mot de passe</p>
    <input type="password" name="userpass1"   id="motdepasse2" placeholder="Mot de passe" />

    <input type="submit" class="btn btn-primary" value="Inscription" id ="buttonier" />

</form>
</div>
    </body>



     <!--Feuilles de style -->

     <style>
        .center-div {
         margin:  auto;
        width: 500px;
        height : 50px;
        padding: 8px 16px; }

        
        .btn{
            font-size: 15px;
            text-transform: uppercase;
        }

        body {
        background-color: var(--bg-main);
        color: var(--color-main);
        font-family: var(--font-main);}


        nav {
        background-color: var(--bg-accent);
        display: flex;
        height: 64px;
        padding: 8px 16px;}


        nav a {
        align-self: center;
        font-size: 14px;
        font-weight: bold;
        letter-spacing: -.2px;
        padding: 0 8px;
        text-transform: uppercase;}


        nav a:after {
        content: '';
        background: transparent;
        display: block;
        height: 1px;}



        nav a:hover {
        text-decoration: none;}



        nav a:hover:after {
         background: var(--color-main);}



        nav a.account {
         margin-left: auto;}


         footer {
        border-top: 1px solid rgba(0, 0, 0, .2);
        margin: 70px 50px 0;
        padding: 32px 0;
        text-align: right;}


        footer strong {
        font-weight: 600;}

        .label-form{
            color: white;
            height : 80px;
            text-transform: uppercase;
            font-size: 25px;
            font-weight: bold;
        }

        .form-check-label{
            color:white;
            font-size: 25px;
            height : 80px;
            text-transform: uppercase;
        }

        .btn{
            font-size: 25px;
            text-transform: uppercase;
        }

        .page-footer {
        background-color: grey;}

        

        .list-Modules{
            color: white;
            height : 50px;
            text-transform: uppercase;
            font-size: 40px;
            font-weight: bold;
        }

        body {
        color:black;
        background-color:white;
        background-image:url(https://enterprisersproject.com/sites/default/files/styles/large/public/images/CIO_IoT_1.png?itok=CZ8aEyn3);}
        

        .grid-container {
        display: grid;
        grid-row-gap: 50px;
        grid-template-columns: auto auto auto;
        background-color: #2196F3;
        padding: 10px;}

        .grid-item {
        background-color: rgba(255, 255, 255, 0.8);
        border: 1px solid rgba(0, 0, 0, 0.8);
        padding: 20px;
        font-size: 15px;
        text-align: center;}


        #account {
        display: flex;
         margin: 22px;}

        #account p {
        font-size: 14px;
         font-weight: 600;
        margin-bottom: 8px;
        margin-top: 20px;}

        #account h2 {
         font-size: 32px;
         font-weight: 600;
         margin-bottom: 8px;}

        #account h3 {
        font-size: 20px;
        margin-bottom: 24px;}

        #account p.valid {
         color: var(--color-valid);}

        #account input.valid {
        border-color: var(--color-valid);}

        #account p.invalid {
         color: var(--color-invalid);}

        #account input.invalid {
        border-color: var(--color-invalid);}

        #account input[type="submit"] {
        margin-top: 24px;}

        #account .account-login {
        align-self: flex-start;
        background-color: #eee;
        flex: 1;
        padding: 34px;}

        #account .account-signin {
         align-self: flex-start;
        background-color: #eee;
        flex: 1;
        padding: 64px;}

        h1 {
        font-size: 42px;
        font-weight: 700;}
         

        h2 {
        font-size: 32px;}

h3 {
  font-size: 20px;
  font-weight: 600;
}

h4 {
  font-size: 18px;
  font-weight: 600;
}

a {
  color: inherit;
  text-decoration: none;
}

a:hover {
  text-decoration: underline;
}

input[type="text"],
input[type="password"],
input[type="mail"],
input[type="number"] {
  border-radius: 0;
  border: 1px solid var(--color-main);
  font-family: var(--font-main);
  height: 32px;
  padding: 18px 10px;
  width: 100%;
}

input[type="submit"] {
  background-color: transparent;
  border: 2px solid var(--color-main);
  display: inline-block;
  font-family: var(--font-main);
  padding: 8px 12px;
}

input[type="submit"]:hover {
  cursor: pointer;
  text-decoration: underline;
}

.box {
  border: 1px solid rgba(0, 0, 0, .2);
  display: inline-block;
  margin: 16px 0;
  padding: 16px;
}

.box.info {
  background: var(--bg-info);
}

.box.error {
  background: var(--bg-error);
}

strong {
  font-weight: 600;
}



* {
  box-sizing: border-box;
}




    </style>
    
</html>
