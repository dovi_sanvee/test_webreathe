<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\module;
use App\Models\historique;

class ModulesController extends Controller
{
    //fonction permettant de lister les modules dans la base de donnes
    public function listModules(){
        // selection de tpute les données de la tables historique et modules 
        $module = module::all();
        $historique=historique::all();
        
        return view('listModules',['module'=>$module,'historique'=>$historique]);
    }
     
    // fonction retourne une vue module
    public function Modules(){
       
        return view('Modules');
    }

    // fonction d'ajout de module et d'historique via le formulaire
    public function addModules(Request $request){
        // stockage dans une variable des fonction issue du formulaire
        $name = request('nom');
        $nom = request('nom');
        $description = request('description');
        //creation de differente variable et affectation de donné
        $date_debut = request('date_debut');
        $etat =  1;
        $donne_envoyee = rand(1,500);
        $valeur_mesuree = rand(1,500);
        $duree_fonctionnement = rand(1,500);
        $date_fin = date('Y-m-d', strtotime( '+'.mt_Rand(0,30).' days'));
        //creation d'une nouvelle ligne de module et insertion de cette ligne
        $Module = new module();
        $Module->nom = $name;
        $Module->description = $description;
        $Module->date_debut = $date_debut;
        //creation d'une nouvelle ligne d'historique et insertion de cette ligne 
        $Historique = new historique();
        $Historique->fk_nom = $nom;
        $Historique->valeur_mesuree_actuelle= $valeur_mesuree;
        $Historique->etat_de_marche = $etat;
        $Historique->fk_date_debut = $date_debut;
        $Historique->donnee_envoyee =    $donne_envoyee;
        $Historique->duree_fonctionnement = $duree_fonctionnement;
        $Historique->date_fin = $date_fin;
        //souvegarde des lignes respectifs dans leur table
        $Module->save();
        $Historique->save();
        
        return back();
    }
    
    
    
}
