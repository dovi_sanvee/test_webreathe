<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Historiques extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //creation de la table historiques

        
        Schema::create('historiques', function (Blueprint $table) {
            $table->increments('fk_id');
            $table->string('fk_nom');
            $table->foreign('fk_nom')->references('nom')->on('modules');
            $table->date('fk_date_debut');
            $table->foreign('fk_date_debut')->references('date_debut')->on('modules');
            $table->double('valeur_mesuree_actuelle');
            $table->date('date_fin');
            $table->double('duree_fonctionnement');
            $table->double('donnee_envoyee');
            $table->double('etat_de_marche');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('historiques');
    }
}
