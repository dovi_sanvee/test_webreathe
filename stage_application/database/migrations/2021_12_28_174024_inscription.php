<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Inscription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //creation de la table inscriptions

        Schema::create('inscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('userlastname');
            $table->string('userfirstname');
            $table->string('usermail')->unique();
            $table->string('userpass');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
