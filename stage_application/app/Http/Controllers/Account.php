<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\inscriptions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Controller
{

 //fonction account qui retourne juste la vue welcome
    public function account(){
       
        return view('welcome');
    }
// fonction qui permet l'inscription d'un client 
   public function accountsignin (Request $request){
    // reccuperation des valeur sur le formulaire inscription
    $userpass = request('userpass');
    $userpass1 = request('userpass1');
    $usermail = request('usermail');
    $userfirstname = request('userfirstname');
    $usermail = request('usermail');
    $userlastname = request('userlastname');
   // creation d'une variable c dans lequel on stocke le nombre de ligne de la table inscriptions
    $c=inscriptions::all()->count();
  
   //compare le userpass au userpass1 pour s'assurer que le mot de passe a ete bien representé   
    if($userpass==$userpass1 ){
   //boucle for allant de 0 au nombre de ligne de la table inscription
    for($i =1 ; $i<=$c ;$i++ ){
        $userInput = inscriptions::find($i)->usermail;
     // comparaison entre le mail saisi et les mail dans la base de donnée
        if($userInput==$usermail){
            echo"ereur email déja existant";
           return view('welcome');
        }
     }
         //si condition verifier alors creation et insertion d'une nouvelle ligne dans la table inscriptions
            $inscription = new inscriptions();
            $inscription->userfirstname = $userfirstname;
            $inscription->userlastname = $userlastname;
            $inscription->usermail = $usermail;
            $inscription->userpass = $userpass;
            $inscription->save(); 
            return back();
    }
} 



// fonction qui permet la connexion a un compte existant dans la base de donnée
   public function accountlogin(Request $request){
    // recuperation de la valeur du usermail et userpass
    $userpass1 = request('userpass');
    $usermail1 = request('usermail');
    // creation d'une variable c dans lequel on stocke le nombre de ligne de la table inscriptions
    $c=inscriptions::all()->count();
   
    //boucle for allant de 0 au nombre de ligne de la table inscription
    for($i =1 ; $i<=$c ;$i++ ){
       //stockage de chaque userpass et usermaile dans des variable userInput2  et userInput 
        $userInput2 = inscriptions::find($i)->userpass;
       
        $userInput = inscriptions::find($i)->usermail;
        //comparaison du usermail et userpass obtenu sur le formulaire et des usermail et userpass dans la basse de donnée
        if($userInput==$usermail1 and $userInput2==$userpass1){
           //return de la vue modules si la condu=ition est verifier
           return view('Modules');
        }
     }
     // affichage du message 
     echo "mot de passe ou mail incorect";
     //return de la vue welcome si la condition est pas respecté
     return view('welcome');
            
        
   }


   // fonction permettant de deconnecter son, compte
   public function logout() {
  // detruit la session 
    session_destroy();
 //returnla vue  welcome
       return view('welcome');

}

}
