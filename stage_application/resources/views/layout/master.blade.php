<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" >
        <link rel="stylesheet"  type="text/css" href="../../css/app.css">
        <link  rel="stylesheet" href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" >
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
        <title>modules</title>
       
    </head>

    <!--Body -->

    <body>
        <nav class="navbar navbar-dark bg-primary">
            <a class="navbar-brand" href="/Modules">Ajout Modules</a>
            <a class="navbar-brand" href="/listModules">Listes Modules</a>
            <a class="navbar-brand" href="/logout">Log out</a>
        </nav>
         
        @yield('formulaire')
      
    </body>
    
    <!--Feuilles de style -->

    <style>
        .center-div {
         margin:  auto;
        width: 500px;
        height : 50px;
        padding: 8px 16px; }


        body {
        background-color: var(--bg-main);
        color: var(--color-main);
        font-family: var(--font-main);}


        nav {
        background-color: var(--bg-accent);
        display: flex;
        height: 64px;
        padding: 8px 16px;}


        nav a {
        align-self: center;
        font-size: 14px;
        font-weight: bold;
        letter-spacing: -.2px;
        padding: 0 8px;
        text-transform: uppercase;}


        nav a:after {
        content: '';
        background: transparent;
        display: block;
        height: 1px;}



        nav a:hover {
        text-decoration: none;}



        nav a:hover:after {
         background: var(--color-main);}



        nav a.account {
         margin-left: auto;}


         footer {
        border-top: 1px solid rgba(0, 0, 0, .2);
        margin: 70px 50px 0;
        padding: 32px 0;
        text-align: right;}


        footer strong {
        font-weight: 600;}

        .label-form{
            color: black;
            height : 50px;
            text-transform: uppercase;
            font-size: 15px;
            font-weight: bold;
        }

        .form-check-label{
            color:black;
            font-size: 15px;
            height : 20px;
            text-transform: uppercase;
        }
        
        .ajout{
            align-self: flex-start;
            background-color: #eee;
            padding: 34px;}

        .btn{
            font-size: 15px;
            text-transform: uppercase;
        }

        .page-footer {
        background-color: grey;}

        

        .list-Modules{
            color: white;
            height : 50px;
            text-transform: uppercase;
            font-size: 40px;
            font-weight: bold;
        }

        body {
        color:black;
        background-color:white;
        background-image:url(https://enterprisersproject.com/sites/default/files/styles/large/public/images/CIO_IoT_1.png?itok=CZ8aEyn3);}
        

        .grid-container {
        display: grid;
        grid-row-gap: 50px;
        grid-template-columns: auto auto auto;
        background-color: #2196F3;
        padding: 10px;}

        .grid-item {
        background-color: rgba(255, 255, 255, 0.8);
        border: 1px solid rgba(0, 0, 0, 0.8);
        padding: 20px;
        font-size: 15px;
        text-align: center;}
        
    </style>
</html>