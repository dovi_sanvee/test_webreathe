<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class historique extends Model
{
    use HasFactory;
    protected $fillable = ['fk_nom','fk_date_debut','valeur_mesuree_actuelle','duree_fonctionnement','etat_de_marche'];
}
