<?php

namespace Tests\Feature;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class modulesTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_an_order_can_be_store_to_the_database()
    {
       $this->withoutExceptionHandling();

       // ajout d'un module et d'historique via test

          $response =$this->post('Modules',[
           'nom'=>'huawei',
           'description' =>'flex',
           'date_debut'=>Carbon::today(),
             'fk_nom'=>'hp',
             'fk_date_debut'=>Carbon::today(),
             'date_fin'=>Carbon::tomorrow(),
             'valeur_mesuree_actuelle'=>'52',
             'duree_fonctionnement'=>'33',
             'donnee_envoyee'=>'7',
             'etat_de_marche'=>'9'
             
         ]);

           // ajout d'un module et d'historique via test


         $response =$this->post('Modules',[
            'nom'=>'iphone',
            'description' =>'8',
            'date_debut'=>Carbon::today(),
              'fk_nom'=>'iphone',
              'fk_date_debut'=>Carbon::today(),
              'date_fin'=>Carbon::tomorrow(),
              'valeur_mesuree_actuelle'=>'17',
              'duree_fonctionnement'=>'16',
              'donnee_envoyee'=>'75',
              'etat_de_marche'=>'96'
              
          ]);
         $response->assertOk();
      } 

    }

