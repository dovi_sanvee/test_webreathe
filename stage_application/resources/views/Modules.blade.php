@extends('layout/master')

<!-- formulaire d'ajout de module avec une methode post -->
@section('formulaire')
 

<div class="center-div">

<form  class="ajout" method="POST" action="/Modules" >
    @csrf
        
   <div class="form-group">
        <label for="exampleInputName1" class="label-form">Nom</label>
        <input type="text" class="form-control" name="nom" aria-describedby="emailHelp" placeholder="entrer nom du module">
   </div>
   <div class="form-group">
        <label for="exampleInputDescription"class="label-form">description</label>
        <input type="text" class="form-control" name="description" placeholder="entrer la description du modules">
  </div>
 
  <div class="form-group">
        <label for="exampleInputDescription"class="label-form">date debut</label>
        <input type="date" class="form-control" name="date_debut"  placeholder="entrer la date de debut">
  </div>
  
  
  <button type="submit" class="btn btn-primary">ajout modules</button>
         
</form>    

</div>

@endsection